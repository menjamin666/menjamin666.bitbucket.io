canv = document.getElementById("theGame");
ctx = canv.getContext("2d");

// set starting values
var fps = 60;
var rounds = 2
var counter = rounds;
var counter2 = rounds;
var percentCar=5;
var percentCar2=5;
var speed=0;
var speed2=0;
var buildX = 150;
var buildY = 150;
var buildTempX;
var buildTempY;

var p1win = 0;
var p2win = 0;

//Rules
var finish = false;
var speedLiteTurn = 0.3;
var speedBigTurn = 0.4;

$( "#counter" ).text( "Player 1 Round: " + counter )
$( "#counter2" ).text( "Player 2 Round: " + counter2 )

var gameSpeed = setInterval(game, 1000/ fps);

function game(){

    console.log("Speed " + speed)
    percentCar+=speed;
    if(percentCar>100){ 
        percentCar=0;
        counter-=1
        $( "#counter" ).text( "Player 1 Round: " + counter )
    };

    percentCar2+=speed2;
    if(percentCar2>100){ 
        percentCar2=0
        counter2-=1
        $( "#counter2" ).text( "Player 2 Round: " + counter2 )
    };
    
    draw(percentCar, percentCar2);

    if(counter == 0 || counter2 == 0){
        clearInterval(gameSpeed)
        finishGame()
    }else if(percentCar > 22 && percentCar < 24 && percentCar2 > 32 && percentCar2 < 34 ||
            percentCar > 82 && percentCar < 84 && percentCar2 > 72 && percentCar2 < 74 ){
        clearInterval(gameSpeed)
        crashGame()
    }

    function finishGame(){
        finish = true
        ctx.clearRect(0,0,canv.width,canv.height);
        ctx.font = "30px Arial";
        
        if(counter == 0){
            ctx.fillStyle = "cyan";
            ctx.fillText("Player 1 win",170,235);
            p1win++;
        }else if(counter2 == 0){
            ctx.fillStyle = "red";
            ctx.fillText("Player 2 win",170,235);
            p2win++;
        }
        ctx.fillStyle = "#ff00f2";
        ctx.fillText("Press N to restart",170,300);
        counter = rounds;
        counter2 = rounds;

        percentCar = 0;
        percentCar2 = 0;

        $( "#counter" ).text( "Player 1 Round: " + counter )
        $( "#counter2" ).text( "Player 2 Round: " + counter2 )
    }

    function crashGame(){
        finish = true
        ctx.clearRect(0,0,canv.width,canv.height);
        ctx.font = "30px Arial";
            
        ctx.fillStyle = "#ed9404";
        ctx.fillText("Total Crash, no winner",170,235);
        
        ctx.fillText("Press Any to restart",170,300);
        counter = rounds;
        counter2 = rounds;

        percentCar = 0;
        percentCar2 = 0;

        $( "#counter" ).text( "Player 1 Round: " + counter )
        $( "#counter2" ).text( "Player 2 Round: " + counter2 )
    }


    //Build Track
    function forward(tempX, tempY, length, buildDirection){
        ctx.beginPath();
        ctx.moveTo(tempX, tempY);
        if(buildDirection == "down"){
            ctx.lineTo(tempX, tempY+length);    
            buildTempX = tempX
            buildTempY = tempY + length
            ctx.strokeStyle = '#ff7c85';
        }else if(buildDirection == "right"){
            ctx.lineTo(tempX+length, tempY);
            buildTempX = tempX + length
            buildTempY = tempY
            ctx.strokeStyle = '#969aa3';
        }else if(buildDirection == "up"){
            ctx.lineTo(tempX, tempY-length);
            buildTempX = tempX
            buildTempY = tempY - length
            ctx.strokeStyle = '#969aa3';
        }else if(buildDirection == "left"){
            ctx.lineTo(tempX-length, tempY);
            buildTempX = tempX - length
            buildTempY = tempY
            ctx.strokeStyle = '#969aa3';
        }        
        ctx.stroke();
    }
    
    function turnLeft(tempX, tempY, posNegXY){
        ctx.beginPath();
        ctx.moveTo(tempX, tempY);
        if(posNegXY == "down"){
            ctx.quadraticCurveTo(tempX, tempY+50, tempX+50, tempY+50);
            buildTempX = tempX + 50;
            buildTempY = tempY + 50;
        }else if(posNegXY == "right"){
            ctx.quadraticCurveTo(tempX+50, tempY, tempX+50, tempY-50);
            buildTempX = tempX + 50;
            buildTempY = tempY - 50;
        }else if(posNegXY == "up"){
            ctx.quadraticCurveTo(tempX, tempY-50, tempX-50, tempY-50);
            buildTempX = tempX - 50;
            buildTempY = tempY - 50;
        }else if(posNegXY == "left"){
            ctx.quadraticCurveTo(tempX-50, tempY, tempX-50, tempY+50);
            buildTempX = tempX - 50;
            buildTempY = tempY + 50;
        }
        ctx.strokeStyle = '#a9afba';
        ctx.stroke();
    }

    function changeTrack(tempX, tempY, direction){
        ctx.beginPath();
        ctx.moveTo(tempX, tempY);
        if(direction == "right"){
            ctx.bezierCurveTo(tempX, tempY-50, tempX+50, tempY-100, tempX+50, tempY-150);
            buildTempX = tempX + 50
            buildTempY = tempY - 150
        }else if(direction == "left"){
            ctx.bezierCurveTo(tempX, tempY-50, tempX-50, tempY-100, tempX-50, tempY-150);
            buildTempX = tempX - 50
            buildTempY = tempY - 150
        }
        ctx.strokeStyle = '#ffc4f9';
        ctx.stroke();
    }

    // draw the current frame based on sliderValue
    function draw(sliderValue, sliderValue2){
        // redraw path
        ctx.clearRect(0,0,canv.width,canv.height);
        ctx.lineWidth = 30;

        //        console.log("Start 1 x: " + buildTempX + " y: " + buildTempY)
        forward(buildX,buildY,150,"down")
        //        console.log("2 x: " + buildTempX + " y: " + buildTempY)
        turnLeft(buildTempX, buildTempY, "down")
        //        console.log("3 x: " + buildTempX + " y: " + buildTempY)
        forward(buildTempX,buildTempY,170,"right")
        //        console.log("4 x: " + buildTempX + " y: " + buildTempY)
        turnLeft(buildTempX, buildTempY, "right")
        //        console.log("5 x: " + buildTempX + " y: " + buildTempY)
        changeTrack(buildTempX, buildTempY, "right")
        //        console.log("6 x: " + buildTempX + " y: " + buildTempY)
        forward(buildTempX,buildTempY,50,"up")
        //        console.log("7 x: " + buildTempX + " y: " + buildTempY)
        turnLeft(buildTempX,buildTempY,"up")
        //        console.log("8 x: " + buildTempX + " y: " + buildTempY)
        forward(buildTempX,buildTempY,140,"left")
        //        console.log("9 x: " + buildTempX + " y: " + buildTempY)
        forward(buildTempX,buildTempY,130,"left")
        //        console.log("10 x: " + buildTempX + " y: " + buildTempY)
        turnLeft(buildTempX,buildTempY,"left")
        //        console.log("11 x: " + buildTempX + " y: " + buildTempY)
        forward(buildTempX, buildTempY, 250, "down")
        //        console.log("12 x: " + buildTempX + " y: " + buildTempY)
        turnLeft(buildTempX, buildTempY, "down")
        //        console.log("13 x: " + buildTempX + " y: " + buildTempY)
        forward(buildTempX, buildTempY, 140, "right")
        //        console.log("14 x: " + buildTempX + " y: " + buildTempY)
        forward(buildTempX, buildTempY, 130, "right")
        //        console.log("15 x: " + buildTempX + " y: " + buildTempY)
        turnLeft(buildTempX, buildTempY, "right")
        //        console.log("16 x: " + buildTempX + " y: " + buildTempY)
        forward(buildTempX, buildTempY, 50, "up")
        //        console.log("17 x: " + buildTempX + " y: " + buildTempY)
        changeTrack(buildTempX, buildTempY, "left")
        //        console.log("18 x: " + buildTempX + " y: " + buildTempY)
        turnLeft(buildTempX, buildTempY, "up")
        //        console.log("19 x: " + buildTempX + " y: " + buildTempY)
        forward(buildTempX, buildTempY, 170, "left")
        //        console.log("20 x: " + buildTempX + " y: " + buildTempY)
        turnLeft(buildTempX, buildTempY, "left")
        //        console.log("End x: " + buildTempX + " y: " + buildTempY)

        // draw the tracking car
        drawCar(moveWhiteCar(sliderValue,speed), "cyan",rotateCar(sliderValue,speed),"whiteCar.png");
        drawCar(moveRedCar(sliderValue2,speed2), "red" ,rotateCar(sliderValue2,speed2),"redCar.png")

        // draw finish line
        ctx.lineWidth = 10;
        ctx.font = "30px Arial";
        ctx.fillStyle = "white";
        ctx.fillText("Finish",170,235);
        ctx.beginPath();
        ctx.moveTo(0, 225);
        ctx.lineTo(160, 225);
        ctx.strokeStyle = 'white';
        ctx.stroke()
    }

    // draw tracking rect at xy
    function drawCar(point,color,rotation,image){
        var imageObj = new Image();
        imageObj.src = "img/" + image;
        ctx.save();
        ctx.translate(point.x,point.y);
        ctx.rotate(rotation + Math.PI/2.0);
        
        ctx.translate(-point.x, -point.y); 
        ctx.drawImage(imageObj, point.x-20, point.y-10, 40 , 20);
        ctx.restore(); 
        /* 
        if(color == "cyan"){
            ctx.fillStyle="cyan";
            ctx.strokeStyle="gray";
        }else if(color == "red"){
            ctx.fillStyle="red";
            ctx.strokeStyle="black";
        }
        ctx.lineWidth=3;
        ctx.beginPath();
        ctx.rect(point.x-8,point.y-8,15,15);
        ctx.fill();
        ctx.stroke(); */

    }
}

//Controller player 1 and 2
function player1PullUp(){
        while(speed >= 0){
            speed = speed - 0.1
            setTimeout(player1PullUp, 500);        
        }

}

function player1PushDown(){
        console.log("PUSheD: UP " + speed)

        while(speed < 0.4){
                console.log("Speed from pushButton " + speed)
                speed = speed + 0.1
                setTimeout(player1PushDown, 2000);        
        }
}

function player1NewGame(){
    if(finish)
        newGame();
}

function player2PullUp(){
        while(speed2 > 0){
            speed2 = speed2 - 0.1
        }
        speed2 = 0 
}

function player2PushDown(){
        while(speed2 < 0.4){
            speed2 = speed2 + 0.1
        }
}

function player2NewGame(){
    if(finish)
        newGame();
}


function newGame(){
    if(finish){

        ctx.clearRect(0, 0, canv.width, canv.height);
        gameSpeed = setInterval(game,1000/fps);
        
        finish = false;
        draw = false;
        
        document.getElementById("counter").innerHTML = p1win + " win";
        document.getElementById("counter2").innerHTML = p2win + " win";
    }

}

function togglePause() {
    if (!paused){
        clearInterval(gameSpeed);
        paused = true;
        ctx.fillStyle = "yellow";        
        ctx.font = "40px Arial";
        ctx.fillText("Pause",canv.width/3,canv.height/4);
    } 
    else {
        gameSpeed = setInterval(game, 1000/speed);
        paused = false;
    }
}

// Math
    // line: percent is 0-1
    function getLineXYatPercent(startPt,endPt,percent) {
        var dx = endPt.x-startPt.x;
        var dy = endPt.y-startPt.y;
        var X = startPt.x + dx*percent;
        var Y = startPt.y + dy*percent;
        return( {x:X,y:Y} );
    }
    // quadratic bezier: percent is 0-1
    function getQuadraticBezierXYatPercent(startPt,controlPt,endPt,percent) {
        var x = Math.pow(1-percent,2) * startPt.x + 2 * (1-percent) * percent * controlPt.x + Math.pow(percent,2) * endPt.x; 
        var y = Math.pow(1-percent,2) * startPt.y + 2 * (1-percent) * percent * controlPt.y + Math.pow(percent,2) * endPt.y; 
        return( {x:x,y:y} );
    }

    // cubic bezier percent is 0-1
    function getCubicBezierXYatPercent(startPt,controlPt1,controlPt2,endPt,percent){
        var x=CubicN(percent,startPt.x,controlPt1.x,controlPt2.x,endPt.x);
        var y=CubicN(percent,startPt.y,controlPt1.y,controlPt2.y,endPt.y);
        return({x:x,y:y});
    }

    // cubic helper formula at percent distance
    function CubicN(pct, a,b,c,d) {
        var t2 = pct * pct;
        var t3 = t2 * pct;
        return a + (-a * 3 + pct * (3 * a - a * pct)) * pct
        + (3 * b + pct * (-6 * b + b * 3 * pct)) * pct
        + (c * 3 - c * 3 * pct) * t2
        + d * t3;
    }

//Car

function rotateCar(slider,theSpeed){
    var oldSlider

    var rotation = -slider * 0.13
    console.log("SliderValue: " + -slider + "Rotation: " + rotation)

        if(oldSlider == slider){
            return rotation
        }else{

            oldSlider = slider

            if(slider>0 && slider<5){
                return 0        
                }

            else if(slider>5 && slider<10){
                return changeValue(-1, -11)        
            }

            else if(slider>10 && slider<15){
            return 11        
            }

            else if(slider>25 && slider<30){
                return -22
            }

            else if(slider>30 && slider<35){
                return changeValue(-22, -11)
                /* if (rotation>-11){
                    return rotation-0.2
                }
                else{
                    return rotation+0.2
                } */

            }
            else if(slider>35 && slider<40){
                return -11

            }

            else if(slider>40 && slider<45){
                if (rotation>-11){
                    return rotation+0.2
                }
                else{
                    return rotation-0.2
                }

            }

            else if(slider>49 && slider<55){
                return 0

            }

            else if(slider>58 && slider<70){
                return 11

            }

            else if(slider>74 && slider<85){
                return 9

            }

            else if(slider>85 && slider<97){
                return -11

            }

            else if(slider>98 && slider<110){
                return 0

            }
            else{
            return rotation
            }
        }
        
}

function changeValue(startRotation, stopRotation){
    console.log(startRotation + " " +  stopRotation)
    
    if (startRotation > stopRotation){        
        startRotation = startRotation - 0.1
        return startRotation
    }else if(startRotation < stopRotation){
        startRotation = startRotation + 0.1
        return startRotation
    }
    // else{
    //     return valueHave + howFast
    // } 
}

function moveWhiteCar(sliderValue,speed){
    if(sliderValue<5){
        var percent=sliderValue/5;
        return getLineXYatPercent({x:150,y:150},{x:150,y:300},percent);
    }// 2
    else if(sliderValue<10){
        var percent=(sliderValue-5)/5
        if(speed>speedLiteTurn){
            return getQuadraticBezierXYatPercent({x:150,y:350},{x:200,y:450},percent);
            percentCar = percent*5+0
        }else{
            return getQuadraticBezierXYatPercent({x:150,y:300},{x:150,y:350},{x:200,y:350},percent);
        }
    }// 3
    else if(sliderValue<15){
        var percent=(sliderValue-10)/5
        return getLineXYatPercent({x:200,y:350},{x:370,y:350},percent);
    }// 4
    else if(sliderValue<20) {
        var percent=(sliderValue-15)/5
        if(speed>speedLiteTurn){
            return getQuadraticBezierXYatPercent({x:370,y:350},{x:420,y:350},{x:500,y:350},percent);
            percentCar = percent*5+10
        }else{
            return getQuadraticBezierXYatPercent({x:370,y:350},{x:420,y:350},{x:420,y:300},percent);
        }
    }// 5
    else if(sliderValue<25){
        var percent=(sliderValue-20)/5
        return getCubicBezierXYatPercent({x:420,y:300},{x:420,y:250},{x:470,y:200},{x:470,y:150},percent)
    }// 6
    else if(sliderValue<30){
        var percent=(sliderValue-25)/5
        return getLineXYatPercent({x:470,y:150},{x:470,y:100},percent);
    }// 7
    else if(sliderValue<35){
        var percent=(sliderValue-30)/5
        if(speed>speedBigTurn){
            return getQuadraticBezierXYatPercent({x:470,y:100},{x:470,y:50},{x:420,y:0},percent);
            percentCar = percent*5+25
        }else{
            return getQuadraticBezierXYatPercent({x:470,y:100},{x:470,y:50},{x:420,y:50},percent);
        }
    }// 8 
    else if(sliderValue<40){
        var percent=(sliderValue-35)/5
        return getLineXYatPercent({x:420,y:50},{x:280,y:50},percent);
    }// 9 
    else if(sliderValue<45){
        var percent=(sliderValue-40)/5
        return getLineXYatPercent({x:280,y:50},{x:150,y:50},percent);
    }// 10 
    else if(sliderValue<50){
        var percent=(sliderValue-45)/5
        if(speed>speedBigTurn){
            percentCar = percent*5+40
            return getQuadraticBezierXYatPercent({x:150,y:50},{x:10,y:50},{x:-10,y:100},percent);
        }else{
            return getQuadraticBezierXYatPercent({x:150,y:50},{x:100,y:50},{x:100,y:100},percent);
        }
    }// 11
    else if(sliderValue<55){
        var percent=(sliderValue-50)/5
        return getLineXYatPercent({x:100,y:100},{x:100,y:350},percent);
    }// 12
    else if(sliderValue<60){
        var percent=(sliderValue-55)/5
        if(speed>speedBigTurn){
            percentCar = percent*5+50
            return getQuadraticBezierXYatPercent({x:100,y:350},{x:100,y:450},{x:150,y:500},percent);
        }else{
            return getQuadraticBezierXYatPercent({x:100,y:350},{x:100,y:400},{x:150,y:400},percent);
        }
    }// 13
    else if(sliderValue<65){
        var percent=(sliderValue-60)/5
        return getLineXYatPercent({x:150,y:400},{x:290,y:400},percent);
    }// 14
    else if(sliderValue<70){
        var percent=(sliderValue-65)/5
        return getLineXYatPercent({x:290,y:400},{x:420,y:400},percent);
    }// 15
    else if(sliderValue<75){
        var percent=(sliderValue-70)/5
        if(speed>speedBigTurn){
            percentCar = percent*5+65
            return getQuadraticBezierXYatPercent({x:420,y:400},{x:520,y:400},{x:600,y:350},percent);
        }else{
            return getQuadraticBezierXYatPercent({x:420,y:400},{x:470,y:400},{x:470,y:350},percent);
        }
    }// 16
    else if(sliderValue<80){
        var percent=(sliderValue-75)/5
        return getLineXYatPercent({x:470,y:350},{x:470,y:300},percent);
    }// 17
    else if(sliderValue<85){
        var percent=(sliderValue-80)/5
        return getCubicBezierXYatPercent({x:470,y:300},{x:470,y:250},{x:420,y:200},{x:420,y:150},percent)
    }// 18
    else if(sliderValue<90){
        var percent=(sliderValue-85)/5
        if(speed>speedLiteTurn){
            percentCar = percent*5+80
            return getQuadraticBezierXYatPercent({x:420,y:150},{x:420,y:50},{x:370,y:0},percent);
        }else{
            return getQuadraticBezierXYatPercent({x:420,y:150},{x:420,y:100},{x:370,y:100},percent);
        }
    }// 19
    else if(sliderValue<95){
        var percent=(sliderValue-90)/5
        return getLineXYatPercent({x:370,y:100},{x:200,y:100},percent);
    }// 20!!
    else {
        var percent=(sliderValue-95)/5
        if(speed>speedLiteTurn){
            percentCar = percent*5+90
            return getQuadraticBezierXYatPercent({x:200,y:100},{x:50,y:100},{x:0,y:150},percent);
        }else{
            return getQuadraticBezierXYatPercent({x:200,y:100},{x:150,y:100},{x:150,y:150},percent);
        }
    }
}

function moveRedCar(sliderValue2,speed2){
    if(sliderValue2<5){
        var percent2=sliderValue2/5;
        return getLineXYatPercent({x:100,y:100},{x:100,y:350},percent2);
    }// 2
    else if(sliderValue2<10){
        var percent2=(sliderValue2-5)/5
        if(speed2>speedBigTurn){
            return getQuadraticBezierXYatPercent({x:100,y:350},{x:100,y:450},{x:150,y:500},percent2);
            percentCar2 = percent2*5+0
        }else{
            return getQuadraticBezierXYatPercent({x:100,y:350},{x:100,y:400},{x:150,y:400},percent2);
        }
    }// 3
    else if(sliderValue2<15){
        var percent2=(sliderValue2-10)/5
        return getLineXYatPercent({x:150,y:400},{x:290,y:400},percent2);
    }// 4
    else if(sliderValue2<20) {
        var percent2=(sliderValue2-15)/5
        return getLineXYatPercent({x:290,y:400},{x:420,y:400},percent2);
    }// 5
    else if(sliderValue2<25){
        var percent2=(sliderValue2-20)/5
        if(speed2>speedBigTurn){
            return getQuadraticBezierXYatPercent({x:420,y:400},{x:500,y:400},{x:550,y:350},percent2);
            percentCar2 = percent2*5+15
        }else{
            return getQuadraticBezierXYatPercent({x:420,y:400},{x:470,y:400},{x:470,y:350},percent2);
        }
    }// 6
    else if(sliderValue2<30){
        var percent2=(sliderValue2-25)/5
        return getLineXYatPercent({x:470,y:350},{x:470,y:300},percent2);
    }// 7
    else if(sliderValue2<35){
        var percent2=(sliderValue2-30)/5
        return getCubicBezierXYatPercent({x:470,y:300},{x:470,y:250},{x:420,y:200},{x:420,y:150},percent2)
    }// 8 
    else if(sliderValue2<40){
        var percent2=(sliderValue2-35)/5
        if(speed2>speedLiteTurn){
            percentCar2 = percent2*5+30
            return getQuadraticBezierXYatPercent({x:420,y:150},{x:420,y:50},{x:370,y:0},percent2);
        }else{
            return getQuadraticBezierXYatPercent({x:420,y:150},{x:420,y:100},{x:370,y:100},percent2);
        }
    }// 9 
    else if(sliderValue2<45){
        var percent2=(sliderValue2-40)/5
        return getLineXYatPercent({x:370,y:100},{x:200,y:100},percent2);
    }// 10 
    else if(sliderValue2<50){
        var percent2=(sliderValue2-45)/5
        if(speed2>speedLiteTurn){
            percentCar2 = percent2*5+40
            return getQuadraticBezierXYatPercent({x:200,y:100},{x:100,y:100},{x:50,y:150},percent2);
        }else{
            return getQuadraticBezierXYatPercent({x:200,y:100},{x:150,y:100},{x:150,y:150},percent2);
        }
    }// 11
    else if(sliderValue2<55){
        var percent2=(sliderValue2-50)/5
        return getLineXYatPercent({x:150,y:150},{x:150,y:300},percent2);
    }// 12
    else if(sliderValue2<60){
        var percent2=(sliderValue2-55)/5
        if(speed2>speedLiteTurn){
            percentCar2 = percent2*5+50
            return getQuadraticBezierXYatPercent({x:150,y:300},{x:150,y:400},{x:200,y:450},percent2);
        }else{
            return getQuadraticBezierXYatPercent({x:150,y:300},{x:150,y:350},{x:200,y:350},percent2);
        }
    }// 13
    else if(sliderValue2<65){
        var percent2=(sliderValue2-60)/5
        return getLineXYatPercent({x:200,y:350},{x:370,y:350},percent2);
    }// 14
    else if(sliderValue2<70){
        var percent2=(sliderValue2-65)/5
        if(speed2>speedLiteTurn){
            percentCar2 = percent2*5+60
            return getQuadraticBezierXYatPercent({x:370,y:350},{x:450,y:350},{x:500,y:300},percent2);
        }else{
            return getQuadraticBezierXYatPercent({x:370,y:350},{x:420,y:350},{x:420,y:300},percent2);
        }
    }// 15
    else if(sliderValue2<75){
        var percent2=(sliderValue2-70)/5
        return getCubicBezierXYatPercent({x:420,y:300},{x:420,y:250},{x:470,y:200},{x:470,y:150},percent2)
    }// 16
    else if(sliderValue2<80){
        var percent2=(sliderValue2-75)/5
        return getLineXYatPercent({x:470,y:150},{x:470,y:100},percent2);
    }// 17
    else if(sliderValue2<85){
        var percent2=(sliderValue2-80)/5
        if(speed2>speedBigTurn){
            percentCar2 = percent2*5+75
            return getQuadraticBezierXYatPercent({x:470,y:100},{x:470,y:0},{x:420,y:-50},percent2);
        }else{
            return getQuadraticBezierXYatPercent({x:470,y:100},{x:470,y:50},{x:420,y:50},percent2);
        }
    }// 18
    else if(sliderValue2<90){
        var percent2=(sliderValue2-85)/5
        return getLineXYatPercent({x:420,y:50},{x:280,y:50},percent2);
    }// 19
    else if(sliderValue2<95){
        var percent2=(sliderValue2-90)/5
        return getLineXYatPercent({x:280,y:50},{x:150,y:50},percent2);
    }// 20
    else {
        var percent2=(sliderValue2-95)/5
        if(speed2>speedBigTurn){
            percentCar2 = percent2*5+90
            return getQuadraticBezierXYatPercent({x:150,y:50},{x:50,y:50},{x:0,y:100},percent2);
        }else{
            return getQuadraticBezierXYatPercent({x:150,y:50},{x:100,y:50},{x:100,y:100},percent2);
        }
    }
}